/**
 * Remove status message by clicking on the close button.
 */
(function($, Drupal) {
  Drupal.behaviors.betterStatusMessages = {
    attach: function(context, settings) {
      $('#js-close-status-message', context).click(function() {
        var elem = $('#js-status-message');
        elem.remove();
      });
    }
  };
})(jQuery, Drupal);